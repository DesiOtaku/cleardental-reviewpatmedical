import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: allergyPage

    header: Rectangle { //just as a margin
        height: 10
    }

    GitManager {id: gitManager}

    TextFileManager {id: textManager}

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: allergyGrid
            columns: 3
            anchors.centerIn: parent
            //rowSpacing: firstName.height / 2

            Label {
                font.pointSize: 16
                text: "Allergy"
            }
            Label {
                font.pointSize: 16
                text: "Reaction"
            }

            Label { //just for placeholder to make a new row

            }

            Component {
                id: textFieldComp
                TextField {

                }
            }

            Component {
                id: comboBoxComp
                ComboBox {
                    property var setMe;
                    model: ["Unknown", "Mild / Rash" , "Moderate", "Severe"]
                    editable: true
                    Component.onCompleted: {
                        if(setMe) {
                            var setIndex = find(setMe);
                            if(setIndex >-1) {
                                currentIndex = setIndex;
                            } else {
                                model =  [setMe,"Unknown", "Mild / Rash" , "Moderate", "Severe"];
                                currentIndex=0;
                            }
                        }
                    }
                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    onClicked: {
                        text = "Kill me now!";
                        allergyGrid.killTheZombie();
                    }
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile("allergies.json"));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    textFieldComp.createObject(allergyGrid,{"text":obj["AllergyName"]});
                    comboBoxComp.createObject(allergyGrid,{"setMe":obj["AllergyReaction"]});
                    deleteButton.createObject(allergyGrid,{});
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(allergyGrid,{"placeholderText":"Allergy Name"});
                comboBoxComp.createObject(allergyGrid,{"placeholderText":"Condition Information"});
                deleteButton.createObject(allergyGrid,{"placeholderText":"Condition Information"});
            }

            function killTheZombie() {
                for(var i=3;i<allergyGrid.children.length;i+=3) {
                    var killBut = allergyGrid.children[i+2];
                    if(killBut.text.length > 1) {
                        var kill1 = allergyGrid.children[i];
                        var kill2 = allergyGrid.children[i+1];
                        kill1.destroy();
                        kill2.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }
    }

    Button {
        id: addButton
        icon.name: "list-add"
        font.pointSize: 24
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        width: 50
        height: 50
        background: Rectangle {
            color: addButton.down ? "#17FF1a" : "#0b930b"
            radius: 50
        }
        onClicked: allergyGrid.makeNewRow();

    }

    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            onClicked: {
                var jsonData = [];
                for(var i=3;i<allergyGrid.children.length;i+=3) {
                    if(allergyGrid.children[i].text.length > 2) {
                        var addMe = {"AllergyName": allergyGrid.children[i].text,
                            "AllergyReaction": allergyGrid.children[i+1].editText};
                        jsonData.push(addMe);
                    }
                }
                var jsonString= JSON.stringify(jsonData, null, '\t');
                textManager.saveFile("allergies.json",jsonString);
                gitManager.commitData("Updated allergies");
                startConfirmGood();
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }
}
