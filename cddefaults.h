#ifndef CDDEFAULTS_H
#define CDDEFAULTS_H

#include <QObject>

class CDDefaults : public QObject
{
    Q_OBJECT
public:
    explicit CDDefaults(QObject *parent = nullptr);

    static QString getDefaultPatDir();

signals:

public slots:
};

#endif // CDDEFAULTS_H
