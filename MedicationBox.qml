import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

TextField {
    id: medBoxRoot

    signal newDrugType(string drugType)

    property var drugName;

    onTextEdited: {
        if(text.length > 2) {
            listPopup.updateList(text);
            if(!listPopup.visible) {
                listPopup.open();
            }
        } else {
            listPopup.close();
        }
    }


    Popup {
        id: listPopup
        property var fullList: [];
        width: parent.width
        height: 300
        x: 0
        y:  parent.height

        ListModel {id:drugListModel}

        ListView {
            model: drugListModel
            width: parent.width
            height: parent.height
            delegate: Button {
                text: drugName
                width: parent.width
                flat: true
                onClicked: {
                    medBoxRoot.text = drugName;
                    newDrugType(whatFor);
                    listPopup.close();
                }
            }
            clip: true
        }

        function loadSmallList() {
            var shortXhr = new XMLHttpRequest;
            shortXhr.open("GET", "qrc:/druglist/shortList.txt",false);
            shortXhr.send(null);
            var lines = shortXhr.responseText.split('\n');
            lines.sort();
            listPopup.fullList = lines;
        }

        function loadBigList() {
            var shortXhr = new XMLHttpRequest;
            shortXhr.open("GET", "qrc:/druglist/fullDrugList.txt",false);
            shortXhr.send(null);
            var lines = shortXhr.responseText.split('\n');
            listPopup.fullList = lines;
        }


        function updateList(currentInput) {
            drugListModel.clear();
            for(var i=0;i<fullList.length;i++) {
                if(fullList[i].toUpperCase().includes(currentInput.toUpperCase())) {
                    var theLine = fullList[i].split('@');
                    drugListModel.append({"drugName": theLine[0],"whatFor": theLine[1]});
                }
            }
        }


        Component.onCompleted: loadBigList();


    }

}
