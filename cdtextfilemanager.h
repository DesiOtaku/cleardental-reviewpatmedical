#ifndef CDTEXTFILEMANAGER_H
#define CDTEXTFILEMANAGER_H

#include <QObject>

class CDTextfileManager : public QObject
{
    Q_OBJECT
public:
    explicit CDTextfileManager(QObject *parent = nullptr);

signals:

public slots:
    static void saveFile(QString getFileName, QString getFileText);
    static QString readFile(QString getFileName);

};

#endif // CDTEXTFILEMANAGER_H
