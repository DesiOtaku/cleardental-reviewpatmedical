#ifndef CDGITMANAGER_H
#define CDGITMANAGER_H

#include <QObject>

class CDGitManager : public QObject
{
    Q_OBJECT
public:
    explicit CDGitManager(QObject *parent = nullptr);

signals:

public slots:
    static QString commitData(QString patientName, QString comment);
    static QString commitData(QString comment);
};

#endif // CDGITMANAGER_H
