import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: conditionPage

    header: Rectangle { //just as a margin
        height: 10
    }

    GitManager {id: gitManager}

    TextFileManager {id: textManager}

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: conditionGrid
            columns: 3
            anchors.centerIn: parent
            //rowSpacing: firstName.height / 2

            Label {
                font.pointSize: 16
                text: "Condition Name"
            }
            Label {
                font.pointSize: 16
                text: "Additional information"
            }

            Label { //just for placeholder to make a new row

            }

            Component {
                id: textFieldComp
                TextField {

                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    onClicked: {
                        text = "Kill me now!";
                        conditionGrid.killTheZombie();
                    }
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile("conditions.json"));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    textFieldComp.createObject(conditionGrid,{"text":obj.ConditionName});
                    textFieldComp.createObject(conditionGrid,{"text":obj.ConditionInfo});
                    deleteButton.createObject(conditionGrid,{"placeholderText":""});
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(conditionGrid,{"placeholderText":"Condition Name"});
                textFieldComp.createObject(conditionGrid,{"placeholderText":"Condition Information"});
                deleteButton.createObject(conditionGrid,{"placeholderText":"Condition Information"});
            }

            function killTheZombie() {
                for(var i=3;i<conditionGrid.children.length;i+=3) {
                    var killBut = conditionGrid.children[i+2];
                    if(killBut.text.length > 1) {
                        var kill1 = conditionGrid.children[i];
                        var kill2 = conditionGrid.children[i+1];
                        kill1.destroy();
                        kill2.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }
    }

    Button {
        id: addButton
        icon.name: "list-add"
        font.pointSize: 24
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        width: 50
        height: 50
        background: Rectangle {
            color: addButton.down ? "#17FF1a" : "#0b930b"
            radius: 50
        }
        onClicked: conditionGrid.makeNewRow();

    }

    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            onClicked: {
                var jsonData = [];
                for(var i=3;i<conditionGrid.children.length;i+=3) {
                    if(conditionGrid.children[i].text.length > 2) {
                        var addMe = {"ConditionName": conditionGrid.children[i].text,
                            "ConditionInfo": conditionGrid.children[i+1].text};
                        jsonData.push(addMe);
                    }
                }
                var jsonString= JSON.stringify(jsonData, null, '\t');
                textManager.saveFile("conditions.json",jsonString);
                gitManager.commitData("Updated medical conditions");
                startConfirmGood();
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }
}
