import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Button {
    id: saveButton
    property real iconSizer: height
    property var oldColor
    property var nameItems
    text: "Save and Commit"
    icon.name: "document-save"
    icon.height: iconSizer
    icon.width: iconSizer

    function startConfirmGood() {
        oldColor = saveButton.icon.color;
        confirmGood.start();
    }

    background: Rectangle {
        opacity: enabled ? 1 : 0.3
        border.color: saveButton.down ?"#82faff":"#86c1ff"
        color: saveButton.down ?"#82faff":"#86c1ff" ;
        border.width: 1

    }

    SequentialAnimation {
        id: confirmGood
        PropertyAnimation  {
            target: saveButton
            property: "iconSizer"
            from: saveButton.height
            to: 0
            duration: 200
        }
        ScriptAction {
            script: {
                saveButton.icon.name = "dialog-ok"
                saveButton.icon.color= "green"
            }
        }
        PropertyAnimation {
            target: saveButton
            property: "iconSizer"
            to: saveButton.height
            from: 0
            duration: 200
        }


        PauseAnimation {
            duration: 1000
        }

        PropertyAnimation  {
            target: saveButton
            property: "iconSizer"
            from: saveButton.height
            to: 0
            duration: 200
        }
        ScriptAction {
            script: {
                saveButton.icon.name = "document-save";
                saveButton.icon.color= oldColor;
            }
        }
        PropertyAnimation {
            target: saveButton
            property: "iconSizer"
            to: saveButton.height
            from: 0
            duration: 200
        }
    }
}
