#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "cdtextfilemanager.h"
#include "cdgitmanager.h"

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<CDTextfileManager>("dental.clear", 1, 0, "TextFileManager");
    qmlRegisterType<CDGitManager>("dental.clear", 1, 0, "GitManager");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("patientFileName",argv[1]);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
