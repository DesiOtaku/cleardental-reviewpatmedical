import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import QtQuick.VirtualKeyboard 2.2

ApplicationWindow {
    id: rootWin
    visible: true
    width: 1024
    height: 768
    title: qsTr("Review medical history")


    header: TabBar {
        id: bar
        width: parent.width

        TabButton {
            text: qsTr("Conditions")
        }
        TabButton {
            text: qsTr("Medications")
        }
        TabButton {
            text: qsTr("Allergies")
        }
        TabButton {
            text: qsTr("Past Surgeries")
        }
    }

    SwipeView   {
        id: view
        anchors.top: bar.bottom
        anchors.left: rootWin.contentItem.left
        anchors.right: rootWin.contentItem.right
        anchors.bottom: rootWin.contentItem.bottom
        currentIndex: bar.currentIndex
        interactive: false

        ConditionsPage{}
        MedicationsPage{}
        AllergiesPage{}
        SurgeriesPage{}
    }

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: rootWin.height
        width: rootWin.width
        active: false

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: rootWin.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }
}
