import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: surgeryPage

    header: Rectangle { //just as a margin
        height: 10
    }

    GitManager {id: gitManager}

    TextFileManager {id: textManager}

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width

        GridLayout {
            id: surgeryGrid
            columns: 4
            anchors.centerIn: parent

            Label {
                font.pointSize: 16
                text: "What what done"
            }

            Label {
                font.pointSize: 16
                text: "When"
            }

            Label {
                font.pointSize: 16
                text: "Additional information"
            }

            Label { //just for placeholder for delete button

            }

            Component {
                id: textFieldComp
                TextField {

                }
            }

            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    onClicked: {
                        text = "Kill me now!";
                        surgeryGrid.killTheZombie();
                    }
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile("surgeries.json"));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    textFieldComp.createObject(surgeryGrid,{"text":obj["Procedure"]});
                    textFieldComp.createObject(surgeryGrid,{"text":obj["When"]});
                    textFieldComp.createObject(surgeryGrid,{"text":obj["AdditionalInfo"]});
                    deleteButton.createObject(surgeryGrid,{});
                }
            }

            function makeNewRow() {
                textFieldComp.createObject(surgeryGrid,{"placeholderText":"Procedure done"});
                textFieldComp.createObject(surgeryGrid,{"placeholderText":"When it was done"});
                textFieldComp.createObject(surgeryGrid,{"placeholderText":"More Information"});
                deleteButton.createObject(surgeryGrid,{});
            }

            function killTheZombie() {
                for(var i=4;i<surgeryGrid.children.length;i+=4) {
                    var killBut = surgeryGrid.children[i+3];
                    if(killBut.text.length > 1) {
                        var kill1 = surgeryGrid.children[i];
                        var kill2 = surgeryGrid.children[i+1];
                        var kill3 = surgeryGrid.children[i+2];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }
    }

    Button {
        id: addButton
        icon.name: "list-add"
        font.pointSize: 24
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        width: 50
        height: 50
        background: Rectangle {
            color: addButton.down ? "#17FF1a" : "#0b930b"
            radius: 50
        }
        onClicked: surgeryGrid.makeNewRow();

    }

    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            onClicked: {
                var jsonData = [];
                for(var i=4;i<surgeryGrid.children.length;i+=4) {
                    if(surgeryGrid.children[i].text.length > 2) {
                        var addMe = {"Procedure": surgeryGrid.children[i].text,
                            "When": surgeryGrid.children[i+1].text,
                            "AdditionalInfo": surgeryGrid.children[i+2].text};
                        jsonData.push(addMe);
                    }
                }
                var jsonString= JSON.stringify(jsonData, null, '\t');
                textManager.saveFile("surgeries.json",jsonString);
                gitManager.commitData("Updated surgeries");
                startConfirmGood();
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }
}
