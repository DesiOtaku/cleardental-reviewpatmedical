import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10

SwipeView {

    Page {
        width: parent.width
        Row {
            width: parent.width
            Text {
                text: "Any Skin issues?"
                font.pixelSize: rootWin.height/10
                horizontalAlignment: Text.AlignHCenter
                width: parent.width
            }
        }
    }

}
