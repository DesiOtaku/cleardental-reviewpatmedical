import QtQuick 2.9
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.10
import dental.clear 1.0

Page {
    id: medicationsPage

    header: Rectangle { //just as a margin
        height: 10
    }

    GitManager {id: gitManager}

    TextFileManager {id: textManager}

    ScrollView {
        anchors.fill: parent
        clip: true
        contentWidth: parent.width


        GridLayout {
            id: medGrid
            columns: 6
            anchors.centerIn: parent
            width: parent.width - 10

            Label {
                font.pointSize: 16
                text: "Medication Name"
            }
            Label {
                font.pointSize: 16
                text: "Used for"
            }

            Label {
                font.pointSize: 16
                text: "Rx here"
            }

            Label {
                font.pointSize: 16
                text: "Amount"
            }

            Label {
                font.pointSize: 16
                text: "Schedule"
            }

            Label { //so we don't offset the delete button
                font.pointSize: 16
                text: ""
            }

            Component {
                id: drugNameComp
                MedicationBox {
                    Layout.minimumWidth: 175
                }
            }

            Component {
                id: usedForText
                TextField {

                }
            }

            Component {
                id:isPrescribedHereBox
                CheckBox {
                    enabled: false
                    checkState: Qt.Unchecked
                }
            }

            Component {
                id: amountField
                TextField {

                }
            }

            Component {
                id:scheduleField
                ComboBox {
                    editable: true
                    property var setMe;
                    model: ["QD","BID","TID","QID","QHS","Q4H","Q6H","Q8H","QOD",
                    "PRN","AC","PC","IM","IV","QTT"]
                    Component.onCompleted: {
                        if(setMe) {
                            var setIndex = find(setMe);
                            if(setIndex >-1) {
                                currentIndex = setIndex;
                            } else {
                                model = [setMe, "QD","BID","TID","QID","QHS","Q4H","Q6H","Q8H","QOD",
                                         "PRN","AC","PC","IM","IV","QTT"];
                                currentIndex=0;
                            }
                        }
                    }
                }
            }


            Component {
                id: deleteButton
                Button {
                    icon.name: "list-remove"
                    onClicked: {
                        text = "Kill me now!";
                        medGrid.killTheZombie();
                    }
                }
            }

            Component.onCompleted:  {
                var result = JSON.parse(textManager.readFile("medications.json"));
                for(var i=0;i<result.length;i++) {
                    var obj = result[i];
                    drugNameComp.createObject(medGrid,{"text": obj["DrugName"]});
                    usedForText.createObject(medGrid,{"text": obj["UsedFor"]});
                    isPrescribedHereBox.createObject(medGrid,{"checked": obj["PrescribedHere"]});
                    amountField.createObject(medGrid,{"text": obj["Amount"]});
                    scheduleField.createObject(medGrid,{"setMe": obj["Schedule"]});
                    deleteButton.createObject(medGrid,{});
                }
            }

            function makeNewRow() {
                drugNameComp.createObject(medGrid,{});
                usedForText.createObject(medGrid,{});
                isPrescribedHereBox.createObject(medGrid,{});
                amountField.createObject(medGrid,{});
                scheduleField.createObject(medGrid,{});
                deleteButton.createObject(medGrid,{});
            }

            function killTheZombie() {
                for(var i=6;i<medGrid.children.length;i+=6) {
                    var killBut = medGrid.children[i+5];
                    if(killBut.text.length > 1) {
                        var kill1 = medGrid.children[i];
                        var kill2 = medGrid.children[i+1];
                        var kill3 = medGrid.children[i+2];
                        var kill4 = medGrid.children[i+3];
                        var kill5 = medGrid.children[i+4];
                        kill1.destroy();
                        kill2.destroy();
                        kill3.destroy();
                        kill4.destroy();
                        kill5.destroy();
                        killBut.destroy();
                        return; //probably only one zombie to kill
                    }
                }
            }
        }
    }

    Button {
        id: addButton
        icon.name: "list-add"
        font.pointSize: 24
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.rightMargin: 10
        width: 50
        height: 50
        background: Rectangle {
            color: addButton.down ? "#17FF1a" : "#0b930b"
            radius: 50
        }
        onClicked: medGrid.makeNewRow();

    }

    footer: Row {
        height: rootWin.height / 20
        SaveButton {
            id: saveButton
            height: parent.height
            width: parent.width / 2
            onClicked: {
                var jsonData = [];
                for(var i=6;i<medGrid.children.length;i+=6) {
                    if(medGrid.children[i].text.length > 2) {
                        var addMe = {"DrugName": medGrid.children[i].text,
                            "UsedFor": medGrid.children[i+1].text,
                            "PrescribedHere": medGrid.children[i+2].checked,
                            "Amount": medGrid.children[i+3].text,
                            "Schedule": medGrid.children[i+4].editText};
                        jsonData.push(addMe);
                    }
                }
                var jsonString= JSON.stringify(jsonData, null, '\t');
                textManager.saveFile("medications.json",jsonString);
                gitManager.commitData("Updated medications");
                startConfirmGood();
            }

        }
        DelayButton {
            id: quitButton
            height: parent.height
            width: parent.width / 2
            text: "Hold to Quit"
            delay: 800
            onActivated: Qt.quit()
        }
    }
}
